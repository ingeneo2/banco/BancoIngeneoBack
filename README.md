# BancoIngeneoBack



## Instalación

Para instalar y ejecutar el servidor backend en tu entorno local, sigue estos pasos:

## Clona este repositorio en tu máquina local utilizando el siguiente comando:

```
git clone https://gitlab.com/ingeneo2/banco/BancoIngeneoBack.git
```

mporta el proyecto en tu IDE preferido que admita el desarrollo de aplicaciones Spring Boot.


Configura tu entorno de desarrollo con JDK y Maven si aún no lo has hecho.


Configura la conexión a la base de datos en el archivo application.properties o application.yml, según tu preferencia, proporcionando la URL de tu base de datos, nombre de usuario y contraseña.


## Compila y ejecuta la aplicación desde tu IDE. Alternativamente, puedes usar Maven desde la línea de comandos:

```
mvn spring-boot:run
```
Una vez que el servidor esté en funcionamiento, estará listo para atender las solicitudes de la aplicación frontend y proporcionar los datos necesarios.
