package org.example.bancoingeneoback.Controller;

import org.example.bancoingeneoback.Model.DTO.ClienteDTO;
import org.example.bancoingeneoback.Services.ClienteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ClienteControllerTest {

    @Mock
    private ClienteService clienteService;

    @InjectMocks
    private ClienteController clienteController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCrearCliente() {
        ClienteDTO clienteDTO = new ClienteDTO(); // Crear un objeto clienteDTO para usar en el test
        when(clienteService.crearCliente(clienteDTO)).thenReturn(clienteDTO);

        ResponseEntity<ClienteDTO> response = clienteController.crearCliente(clienteDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(clienteDTO, response.getBody());
    }

    @Test
    public void testObtenerTodosLosClientes() {
        List<ClienteDTO> clientes = new ArrayList<>(); // Crear una lista de clientes para usar en el test
        when(clienteService.obtenerTodosLosClientes()).thenReturn(clientes);

        List<ClienteDTO> response = clienteController.obtenerTodosLosClientes();

        assertEquals(clientes, response);
    }

    @Test
    public void testObtenerClientePorId() {
        Long id = 1L; // ID del cliente a buscar
        ClienteDTO clienteDTO = new ClienteDTO(); // Crear un objeto clienteDTO para usar en el test
        when(clienteService.obtenerClientePorId(id)).thenReturn(clienteDTO);

        ResponseEntity<ClienteDTO> response = clienteController.obtenerClientePorId(id);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clienteDTO, response.getBody());
    }

    @Test
    public void testActualizarCliente() {
        Long id = 1L; // ID del cliente a actualizar
        ClienteDTO clienteDTO = new ClienteDTO(); // Crear un objeto clienteDTO para usar en el test
        when(clienteService.actualizarCliente(id, clienteDTO)).thenReturn(clienteDTO);

        ResponseEntity<ClienteDTO> response = clienteController.actualizarCliente(id, clienteDTO);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clienteDTO, response.getBody());
    }

    @Test
    public void testEliminarCliente() {
        Long id = 1L; // ID del cliente a eliminar

        ResponseEntity<Void> response = clienteController.eliminarCliente(id);

        verify(clienteService, times(1)).eliminarCliente(id);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
