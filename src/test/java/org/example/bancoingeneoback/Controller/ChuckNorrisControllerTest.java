package org.example.bancoingeneoback.Controller;

import org.example.bancoingeneoback.Services.ChuckNorrisService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class ChuckNorrisControllerTest {

    @Mock
    private ChuckNorrisService chuckNorrisService;

    private ChuckNorrisController chuckNorrisController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        chuckNorrisController = new ChuckNorrisController(chuckNorrisService);
    }

    @Test
    void obtenerChisteDeChuckNorris_DeberiaRetornarChisteCuandoChuckNorrisServiceRetornaChiste() {
        // Arrange
        String chiste = "Chuck Norris puede dividir por cero.";
        when(chuckNorrisService.obtenerChisteDeChuckNorris()).thenReturn(chiste);

        // Act
        ResponseEntity<String> responseEntity = chuckNorrisController.obtenerChisteDeChuckNorris();

        // Assert
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(chiste, responseEntity.getBody());
    }
}
