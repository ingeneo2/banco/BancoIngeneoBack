package org.example.bancoingeneoback.Controller;

import org.example.bancoingeneoback.Model.DTO.MovimientoDTO;
import org.example.bancoingeneoback.Services.MovimientoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class MovimientoControllerTest {

    @Mock
    private MovimientoService movimientoService;

    @InjectMocks
    private MovimientoController movimientoController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegistrarMovimiento() {
        Long cuentaId = 1L; // ID de la cuenta asociada al movimiento
        MovimientoDTO movimientoDTO = new MovimientoDTO(); // Crear un objeto movimientoDTO para usar en el test
        when(movimientoService.registrarMovimiento(cuentaId, movimientoDTO)).thenReturn(movimientoDTO);

        ResponseEntity<MovimientoDTO> response = movimientoController.registrarMovimiento(cuentaId, movimientoDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(movimientoDTO, response.getBody());
    }

    @Test
    public void testObtenerTodosLosMovimientos() {
        List<MovimientoDTO> movimientos = new ArrayList<>(); // Crear una lista de movimientos para usar en el test
        when(movimientoService.obtenerTodosLosMovimientos()).thenReturn(movimientos);

        List<MovimientoDTO> response = movimientoController.obtenerTodosLosClientes();

        assertEquals(movimientos, response);
    }

    @Test
    public void testEliminarMovimiento() {
        Long id = 1L; // ID del movimiento a eliminar

        ResponseEntity<Void> response = movimientoController.eliminarCliente(id);

        verify(movimientoService, times(1)).eliminarMovimiento(id);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
