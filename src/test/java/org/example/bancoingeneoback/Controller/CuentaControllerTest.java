package org.example.bancoingeneoback.Controller;

import org.example.bancoingeneoback.Model.DTO.CuentaDTO;
import org.example.bancoingeneoback.Model.DTO.InformeClienteDTO;
import org.example.bancoingeneoback.Services.CuentaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class CuentaControllerTest {

    @Mock
    private CuentaService cuentaService;

    @InjectMocks
    private CuentaController cuentaController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCrearCuenta() {
        Long clienteId = 1L; // ID del cliente asociado a la cuenta
        CuentaDTO cuentaDTO = new CuentaDTO(); // Crear un objeto cuentaDTO para usar en el test
        when(cuentaService.crearCuenta(clienteId, cuentaDTO)).thenReturn(cuentaDTO);

        ResponseEntity<CuentaDTO> response = cuentaController.crearCuenta(clienteId, cuentaDTO);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(cuentaDTO, response.getBody());
    }

    @Test
    public void testObtenerTodasLasCuentas() {
        List<CuentaDTO> cuentas = new ArrayList<>(); // Crear una lista de cuentas para usar en el test
        when(cuentaService.obtenerTodasLasCuentas()).thenReturn(cuentas);

        List<CuentaDTO> response = cuentaController.obtenerTodasLasCuentas();

        assertEquals(cuentas, response);
    }

    @Test
    public void testObtenerCuentaPorId() {
        Long id = 1L; // ID de la cuenta a buscar
        CuentaDTO cuentaDTO = new CuentaDTO(); // Crear un objeto cuentaDTO para usar en el test
        when(cuentaService.obtenerCuentaPorId(id)).thenReturn(cuentaDTO);

        ResponseEntity<CuentaDTO> response = cuentaController.obtenerCuentaPorId(id);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(cuentaDTO, response.getBody());
    }

    @Test
    public void testActualizarCuenta() {
        Long id = 1L; // ID de la cuenta a actualizar
        CuentaDTO cuentaDTO = new CuentaDTO(); // Crear un objeto cuentaDTO para usar en el test
        when(cuentaService.actualizarCuenta(id, cuentaDTO)).thenReturn(cuentaDTO);

        ResponseEntity<CuentaDTO> response = cuentaController.actualizarCuenta(id, cuentaDTO);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(cuentaDTO, response.getBody());
    }

    @Test
    public void testEliminarCuenta() {
        Long id = 1L; // ID de la cuenta a eliminar

        ResponseEntity<Void> response = cuentaController.eliminarCuenta(id);

        verify(cuentaService, times(1)).eliminarCuenta(id);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void testGenerarInformeCliente() {
        Long clienteId = 1L; // ID del cliente para el informe
        Date fechaInicio = new Date(); // Fecha de inicio para el informe
        Date fechaFin = new Date(); // Fecha de fin para el informe
        InformeClienteDTO informeClienteDTO = new InformeClienteDTO(); // Crear un objeto informeClienteDTO para usar en el test
        when(cuentaService.generarInformeCliente(clienteId, fechaInicio, fechaFin)).thenReturn(informeClienteDTO);

        ResponseEntity<InformeClienteDTO> response = cuentaController.generarInformeCliente(clienteId, fechaInicio, fechaFin);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(informeClienteDTO, response.getBody());
    }
}
