package org.example.bancoingeneoback.Services.ServiceImpl;

import org.example.bancoingeneoback.Config.Eceptiones.CuentaNotFoundException;
import org.example.bancoingeneoback.Config.Eceptiones.SaldoInsuficienteException;
import org.example.bancoingeneoback.Model.DTO.ClienteDTO;
import org.example.bancoingeneoback.Model.DTO.CuentaDTO;
import org.example.bancoingeneoback.Model.DTO.MovimientoDTO;
import org.example.bancoingeneoback.Model.Entidades.Cliente;
import org.example.bancoingeneoback.Model.Entidades.Cuenta;
import org.example.bancoingeneoback.Model.Entidades.Movimiento;
import org.example.bancoingeneoback.Model.Enums.Tipo;
import org.example.bancoingeneoback.Repository.CuentaRepository;
import org.example.bancoingeneoback.Repository.MovimientoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class MovimientoServiceImplTest {

    @Mock
    private MovimientoRepository movimientoRepository;

    @Mock
    private CuentaRepository cuentaRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private MovimientoServiceImpl movimientoService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testObtenerTodosLosMovimientos() {
        List<Movimiento> movimientos = new ArrayList<>();
        when(movimientoRepository.findAll()).thenReturn(movimientos);

        List<MovimientoDTO> response = movimientoService.obtenerTodosLosMovimientos();

        assertEquals(new ArrayList<>(), response);
    }


    @Test
    public void testRegistrarMovimientoInvalidCuentaId() {
        Long cuentaId = 1L;
        MovimientoDTO movimientoDTO = new MovimientoDTO();
        movimientoDTO.setTipo(Tipo.CREDITO);
        movimientoDTO.setValor(100.0);

        when(cuentaRepository.findById(cuentaId)).thenReturn(Optional.empty());

        assertThrows(CuentaNotFoundException.class, () -> {
            movimientoService.registrarMovimiento(cuentaId, movimientoDTO);
        });
    }

    @Test
    public void testEliminarMovimiento() {
        Long id = 1L;

        movimientoService.eliminarMovimiento(id);

        verify(cuentaRepository, times(1)).deleteById(id);
    }
}
