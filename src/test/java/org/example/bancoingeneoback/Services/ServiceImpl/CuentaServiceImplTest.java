package org.example.bancoingeneoback.Services.ServiceImpl;

import org.example.bancoingeneoback.Config.Eceptiones.ClienteNotFoundException;
import org.example.bancoingeneoback.Config.Eceptiones.CuentaNotFoundException;
import org.example.bancoingeneoback.Model.DTO.ClienteDTO;
import org.example.bancoingeneoback.Model.DTO.CuentaDTO;
import org.example.bancoingeneoback.Model.DTO.InformeClienteDTO;
import org.example.bancoingeneoback.Model.Entidades.Cliente;
import org.example.bancoingeneoback.Model.Entidades.Cuenta;
import org.example.bancoingeneoback.Model.Entidades.Movimiento;
import org.example.bancoingeneoback.Model.Enums.Tipo;
import org.example.bancoingeneoback.Repository.ClienteRepository;
import org.example.bancoingeneoback.Repository.CuentaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CuentaServiceImplTest {

    @Mock
    private CuentaRepository cuentaRepository;

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private CuentaServiceImpl cuentaService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCrearCuenta() {
        Long clienteId = 1L;
        CuentaDTO cuentaDTO = new CuentaDTO();
        Cliente cliente = new Cliente();
        when(clienteRepository.findById(clienteId)).thenReturn(Optional.of(cliente));
        Cuenta cuenta = new Cuenta();
        when(modelMapper.map(cuentaDTO, Cuenta.class)).thenReturn(cuenta);
        when(cuentaRepository.save(cuenta)).thenReturn(cuenta);
        when(modelMapper.map(cuenta, CuentaDTO.class)).thenReturn(cuentaDTO);

        CuentaDTO response = cuentaService.crearCuenta(clienteId, cuentaDTO);

        assertEquals(cuentaDTO, response);
    }

    @Test
    public void testObtenerTodasLasCuentas() {
        List<Cuenta> cuentas = new ArrayList<>();
        when(cuentaRepository.findAll()).thenReturn(cuentas);

        List<CuentaDTO> response = cuentaService.obtenerTodasLasCuentas();

        assertEquals(new ArrayList<>(), response);
    }

    @Test
    public void testObtenerCuentaPorId() {
        Long id = 1L;
        Cuenta cuenta = new Cuenta();
        when(cuentaRepository.findById(id)).thenReturn(Optional.of(cuenta));
        CuentaDTO cuentaDTO = new CuentaDTO();
        when(modelMapper.map(cuenta, CuentaDTO.class)).thenReturn(cuentaDTO);

        CuentaDTO response = cuentaService.obtenerCuentaPorId(id);

        assertEquals(cuentaDTO, response);
    }

    @Test
    public void testObtenerCuentaPorIdNotFound() {
        Long id = 1L;
        when(cuentaRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(CuentaNotFoundException.class, () -> {
            cuentaService.obtenerCuentaPorId(id);
        });
    }

    @Test
    public void testActualizarCuenta() {
        Long id = 1L;
        CuentaDTO cuentaDTO = new CuentaDTO();
        Cuenta cuenta = new Cuenta();
        when(cuentaRepository.findById(id)).thenReturn(Optional.of(cuenta));
        when(cuentaRepository.save(cuenta)).thenReturn(cuenta);
        when(modelMapper.map(cuenta, CuentaDTO.class)).thenReturn(cuentaDTO);

        CuentaDTO response = cuentaService.actualizarCuenta(id, cuentaDTO);

        assertEquals(cuentaDTO, response);
    }

    @Test
    public void testEliminarCuenta() {
        Long id = 1L;

        cuentaService.eliminarCuenta(id);

        verify(cuentaRepository, times(1)).deleteById(id);
    }


    @Test
    public void test_invalid_clienteId() {
        Long clienteId = 1L;
        Date fechaInicio = new Date();
        Date fechaFin = new Date();

        when(clienteRepository.findById(clienteId)).thenReturn(Optional.empty());

        assertThrows(ClienteNotFoundException.class, () -> {
            cuentaService.generarInformeCliente(clienteId, fechaInicio, fechaFin);
        });
    }

}
