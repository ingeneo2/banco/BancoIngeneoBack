package org.example.bancoingeneoback.Services.ServiceImpl;


import org.example.bancoingeneoback.Config.Constantes;
import org.example.bancoingeneoback.Config.Eceptiones.ChuckNorrisApiException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class ChuckNorrisServiceImplTest {

    @Mock
    private RestTemplate restTemplate;

    private ChuckNorrisServiceImpl chuckNorrisService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        chuckNorrisService = new ChuckNorrisServiceImpl(restTemplate);
    }

    @Test
    void obtenerChisteDeChuckNorris_DeberiaRetornarChisteCuandoRespuestaEsOk() {
        String chiste = "{\"value\": \"Chuck Norris puede dividir por cero.\"}";
        ResponseEntity<String> responseEntity = new ResponseEntity<>(chiste, HttpStatus.OK);
        when(restTemplate.getForEntity(Constantes.CHUCK_NORRIS_API_URL, String.class)).thenReturn(responseEntity);

        String resultado = chuckNorrisService.obtenerChisteDeChuckNorris();

        assertEquals("Chuck Norris puede dividir por cero.", resultado);
    }

    @Test
    void obtenerChisteDeChuckNorris_DeberiaLanzarExcepcionCuandoRespuestaNoEsOk() {
        ResponseEntity<String> responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        when(restTemplate.getForEntity(Constantes.CHUCK_NORRIS_API_URL, String.class)).thenReturn(responseEntity);

        assertThrows(ChuckNorrisApiException.class, () -> chuckNorrisService.obtenerChisteDeChuckNorris());
    }
}
