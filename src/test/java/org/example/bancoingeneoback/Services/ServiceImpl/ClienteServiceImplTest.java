package org.example.bancoingeneoback.Services.ServiceImpl;

import org.example.bancoingeneoback.Config.Eceptiones.ClienteNotFoundException;
import org.example.bancoingeneoback.Model.DTO.ClienteDTO;
import org.example.bancoingeneoback.Model.Entidades.Cliente;
import org.example.bancoingeneoback.Repository.ClienteRepository;
import org.example.bancoingeneoback.Services.ClienteService;
import org.example.bancoingeneoback.Services.ServiceImpl.ClienteServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class ClienteServiceImplTest {

    @Mock
    private ClienteRepository clienteRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private ClienteServiceImpl clienteService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCrearCliente() {
        ClienteDTO clienteDTO = new ClienteDTO();
        Cliente cliente = new Cliente();
        when(modelMapper.map(clienteDTO, Cliente.class)).thenReturn(cliente);
        when(clienteRepository.save(cliente)).thenReturn(cliente);
        when(modelMapper.map(cliente, ClienteDTO.class)).thenReturn(clienteDTO);

        ClienteDTO response = clienteService.crearCliente(clienteDTO);

        assertEquals(clienteDTO, response);
    }

    @Test
    public void testObtenerTodosLosClientes() {
        List<Cliente> clientes = new ArrayList<>();
        when(clienteRepository.findAll()).thenReturn(clientes);

        List<ClienteDTO> response = clienteService.obtenerTodosLosClientes();

        assertEquals(new ArrayList<>(), response);
    }

    @Test
    public void testObtenerClientePorId() {
        Long id = 1L;
        Cliente cliente = new Cliente();
        when(clienteRepository.findById(id)).thenReturn(Optional.of(cliente));
        ClienteDTO clienteDTO = new ClienteDTO();
        when(modelMapper.map(cliente, ClienteDTO.class)).thenReturn(clienteDTO);

        ClienteDTO response = clienteService.obtenerClientePorId(id);

        assertEquals(clienteDTO, response);
    }

    @Test
    public void testObtenerClientePorIdNotFound() {
        Long id = 1L; // ID del cliente a buscar
        when(clienteRepository.findById(id)).thenReturn(Optional.empty());

        assertThrows(ClienteNotFoundException.class, () -> {
            clienteService.obtenerClientePorId(id);
        });
    }

    @Test
    public void testActualizarCliente() {
        Long id = 1L;
        ClienteDTO clienteDTO = new ClienteDTO();
        Cliente cliente = new Cliente();
        when(clienteRepository.findById(id)).thenReturn(Optional.of(cliente));
        when(clienteRepository.save(cliente)).thenReturn(cliente);
        when(modelMapper.map(cliente, ClienteDTO.class)).thenReturn(clienteDTO);

        ClienteDTO response = clienteService.actualizarCliente(id, clienteDTO);

        assertEquals(clienteDTO, response);
    }

    @Test
    public void testEliminarCliente() {
        Long id = 1L;

        clienteService.eliminarCliente(id);

        verify(clienteRepository, times(1)).deleteById(id);
    }
}
