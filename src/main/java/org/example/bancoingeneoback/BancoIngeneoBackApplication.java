package org.example.bancoingeneoback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoIngeneoBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoIngeneoBackApplication.class, args);
	}

}
