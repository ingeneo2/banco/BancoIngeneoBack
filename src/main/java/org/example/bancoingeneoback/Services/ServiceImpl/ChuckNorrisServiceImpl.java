package org.example.bancoingeneoback.Services.ServiceImpl;

import org.example.bancoingeneoback.Config.Constantes;
import org.example.bancoingeneoback.Config.Eceptiones.ChuckNorrisApiException;
import org.example.bancoingeneoback.Services.ChuckNorrisService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ChuckNorrisServiceImpl implements ChuckNorrisService {

     private final RestTemplate restTemplate;

    @Autowired
    public ChuckNorrisServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public String obtenerChisteDeChuckNorris() {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(Constantes.CHUCK_NORRIS_API_URL, String.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            JSONObject jsonObject = new JSONObject(responseEntity.getBody());
            return jsonObject.getString(Constantes.KEY);
        } else {
            throw new ChuckNorrisApiException(Constantes.ERROR_OBTENER_CHISTE);
        }
    }
}

