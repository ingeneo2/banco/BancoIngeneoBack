package org.example.bancoingeneoback.Services.ServiceImpl;

import org.example.bancoingeneoback.Config.Eceptiones.ClienteNotFoundException;
import org.example.bancoingeneoback.Config.Eceptiones.CuentaNotFoundException;
import org.example.bancoingeneoback.Model.DTO.ClienteDTO;
import org.example.bancoingeneoback.Model.DTO.CuentaDTO;
import org.example.bancoingeneoback.Model.DTO.InformeClienteDTO;
import org.example.bancoingeneoback.Model.Entidades.Cliente;
import org.example.bancoingeneoback.Model.Entidades.Cuenta;
import org.example.bancoingeneoback.Model.Entidades.Movimiento;
import org.example.bancoingeneoback.Model.Enums.Tipo;
import org.example.bancoingeneoback.Repository.ClienteRepository;
import org.example.bancoingeneoback.Repository.CuentaRepository;
import org.example.bancoingeneoback.Services.CuentaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CuentaServiceImpl implements CuentaService {

    @Autowired
    private CuentaRepository cuentaRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public CuentaDTO crearCuenta(Long clienteId, CuentaDTO cuentaDTO) {
        Cliente cliente = clienteRepository.findById(clienteId)
                .orElseThrow(() -> new ClienteNotFoundException(clienteId));
        Cuenta cuenta = modelMapper.map(cuentaDTO, Cuenta.class);
        cuenta.setCliente(cliente);
        cuenta = cuentaRepository.save(cuenta);
        return modelMapper.map(cuenta, CuentaDTO.class);
    }
    @Override
    public List<CuentaDTO> obtenerTodasLasCuentas() {

        List<Cuenta> cuentas = cuentaRepository.findAll();

        List<CuentaDTO> cuentasDto = new ArrayList<>();

        for (Cuenta cuenta : cuentas) {
            cuentasDto.add(modelMapper.map(cuenta, CuentaDTO.class));
        }

        return cuentasDto;

    }
    public CuentaDTO obtenerCuentaPorId(Long id) {
        Cuenta cuenta = cuentaRepository.findById(id)
                .orElseThrow(() -> new CuentaNotFoundException(id));
        return modelMapper.map(cuenta, CuentaDTO.class);
    }

    public CuentaDTO actualizarCuenta(Long id, CuentaDTO cuentaDTO) {
        Cuenta cuenta = cuentaRepository.findById(id)
                .orElseThrow(() -> new CuentaNotFoundException(id));
        cuenta.setNumero(cuentaDTO.getNumero());
        cuenta.setSaldo(cuentaDTO.getSaldo());
        cuenta = cuentaRepository.save(cuenta);
        return modelMapper.map(cuenta, CuentaDTO.class);
    }

    public void eliminarCuenta(Long id) {
        cuentaRepository.deleteById(id);
    }

    public InformeClienteDTO generarInformeCliente(Long clienteId, Date fechaInicio, Date fechaFin) {
        Cliente cliente = clienteRepository.findById(clienteId)
                .orElseThrow(() -> new ClienteNotFoundException(clienteId));

        List<Cuenta> cuentas = cuentaRepository.findByCliente(cliente);

        double totalDebitos = 0.0;
        double totalCreditos = 0.0;

        List<CuentaDTO> cuentasDTO = new ArrayList<>();

        for (Cuenta cuenta : cuentas) {
            CuentaDTO cuentaDTO = modelMapper.map(cuenta, CuentaDTO.class);

            double saldo = cuenta.getSaldo();

            List<Movimiento> movimientos = cuenta.getMovimientos();
            for (Movimiento movimiento : movimientos) {
                if (movimiento.getFecha().after(fechaInicio) && movimiento.getFecha().before(fechaFin)) {
                    if (movimiento.getTipo() == Tipo.DEBITO) {
                        totalDebitos += movimiento.getValor();
                        saldo -= movimiento.getValor();
                    } else {
                        totalCreditos += movimiento.getValor();
                        saldo += movimiento.getValor();
                    }
                }
            }

            cuentaDTO.setSaldo(saldo);
            cuentasDTO.add(cuentaDTO);
        }

        InformeClienteDTO informeClienteDTO = new InformeClienteDTO();
        informeClienteDTO.setCliente(modelMapper.map(cliente, ClienteDTO.class));
        informeClienteDTO.setCuentas(cuentasDTO);
        informeClienteDTO.setTotalDebitos(totalDebitos);
        informeClienteDTO.setTotalCreditos(totalCreditos);

        return informeClienteDTO;
    }

}

