package org.example.bancoingeneoback.Services.ServiceImpl;

import org.example.bancoingeneoback.Config.Constantes;
import org.example.bancoingeneoback.Config.Eceptiones.CuentaNotFoundException;
import org.example.bancoingeneoback.Config.Eceptiones.SaldoInsuficienteException;
import org.example.bancoingeneoback.Model.DTO.MovimientoDTO;
import org.example.bancoingeneoback.Model.Entidades.Cuenta;
import org.example.bancoingeneoback.Model.Entidades.Movimiento;
import org.example.bancoingeneoback.Model.Enums.Tipo;
import org.example.bancoingeneoback.Repository.CuentaRepository;
import org.example.bancoingeneoback.Repository.MovimientoRepository;
import org.example.bancoingeneoback.Services.MovimientoService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MovimientoServiceImpl implements MovimientoService {

    @Autowired
    private MovimientoRepository movimientoRepository;

    @Autowired
    private CuentaRepository cuentaRepository;

    @Autowired
    private ModelMapper modelMapper;
    @Override
    public List<MovimientoDTO> obtenerTodosLosMovimientos() {

        List<Movimiento> movimientos = movimientoRepository.findAll();

        List<MovimientoDTO> movimientosDto = new ArrayList<>();

        for (Movimiento movimiento : movimientos) {
            movimientosDto.add(modelMapper.map(movimiento, MovimientoDTO.class));
        }

        return movimientosDto;

    }
    @Override
    public MovimientoDTO registrarMovimiento(Long cuentaId, MovimientoDTO movimientoDTO) {
        Cuenta cuenta = cuentaRepository.findById(cuentaId)
                .orElseThrow(() -> new CuentaNotFoundException(cuentaId));

        double nuevoSaldo;
        if (movimientoDTO.getTipo() == Tipo.CREDITO) {
            nuevoSaldo = cuenta.getSaldo() - movimientoDTO.getValor();
        } else {
            nuevoSaldo = cuenta.getSaldo() + movimientoDTO.getValor();
        }

        if (nuevoSaldo < 0) {
            throw new SaldoInsuficienteException(Constantes.SALDO_NEGATIVO);
        }

        Movimiento movimiento = modelMapper.map(movimientoDTO, Movimiento.class);
        movimiento.setFecha(new Date());
        movimiento.setCuenta(cuenta);

        movimiento = movimientoRepository.save(movimiento);


        cuenta.setSaldo(nuevoSaldo);
        cuentaRepository.save(cuenta);

        return modelMapper.map(movimiento, MovimientoDTO.class);
    }
    public void eliminarMovimiento(Long id) {
        cuentaRepository.deleteById(id);
    }
}

