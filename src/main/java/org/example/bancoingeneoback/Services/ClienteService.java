package org.example.bancoingeneoback.Services;

import org.example.bancoingeneoback.Model.DTO.ClienteDTO;

import java.util.List;

public interface ClienteService {
    ClienteDTO crearCliente(ClienteDTO clienteDTO);
    ClienteDTO obtenerClientePorId(Long id);
    ClienteDTO actualizarCliente(Long id, ClienteDTO clienteDTO);
    void eliminarCliente(Long id);

    List<ClienteDTO> obtenerTodosLosClientes();
}

