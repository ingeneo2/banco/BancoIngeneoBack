package org.example.bancoingeneoback.Services;

import org.example.bancoingeneoback.Model.DTO.ClienteDTO;
import org.example.bancoingeneoback.Model.DTO.CuentaDTO;
import org.example.bancoingeneoback.Model.DTO.InformeClienteDTO;

import java.util.Date;
import java.util.List;

public interface CuentaService {
    CuentaDTO crearCuenta(Long clienteId, CuentaDTO cuentaDTO);


    CuentaDTO obtenerCuentaPorId(Long id);
    CuentaDTO actualizarCuenta(Long id, CuentaDTO cuentaDTO);
    void eliminarCuenta(Long id);

    InformeClienteDTO generarInformeCliente(Long clienteId, Date fechaInicio, Date fechaFin);

    List<CuentaDTO> obtenerTodasLasCuentas();
}

