package org.example.bancoingeneoback.Services;

import org.example.bancoingeneoback.Model.DTO.MovimientoDTO;

import java.util.List;

public interface MovimientoService {

    MovimientoDTO registrarMovimiento(Long cuentaId, MovimientoDTO movimientoDTO);

    List<MovimientoDTO> obtenerTodosLosMovimientos();

    void eliminarMovimiento(Long id);
}

