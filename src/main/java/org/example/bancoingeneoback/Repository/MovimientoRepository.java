package org.example.bancoingeneoback.Repository;

import org.example.bancoingeneoback.Model.Entidades.Movimiento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovimientoRepository extends JpaRepository<Movimiento, Long> {
}
