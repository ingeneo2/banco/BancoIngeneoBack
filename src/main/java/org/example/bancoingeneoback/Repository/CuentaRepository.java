package org.example.bancoingeneoback.Repository;

import org.example.bancoingeneoback.Model.Entidades.Cliente;
import org.example.bancoingeneoback.Model.Entidades.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CuentaRepository extends JpaRepository<Cuenta, Long> {
    List<Cuenta> findByCliente(Cliente cliente);
}
