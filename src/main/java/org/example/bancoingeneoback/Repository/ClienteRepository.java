package org.example.bancoingeneoback.Repository;

import org.example.bancoingeneoback.Model.Entidades.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
