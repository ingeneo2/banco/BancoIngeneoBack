package org.example.bancoingeneoback.Model.DTO;

import lombok.Data;

import java.util.List;

@Data
public class InformeClienteDTO {

    private ClienteDTO cliente;
    private List<CuentaDTO> cuentas;
    private double totalDebitos;
    private double totalCreditos;
}

