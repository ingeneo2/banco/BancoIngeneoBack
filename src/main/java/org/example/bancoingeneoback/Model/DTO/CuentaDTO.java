package org.example.bancoingeneoback.Model.DTO;

import lombok.Data;

@Data
public class CuentaDTO {
    private Long id;
    private String numero;
    private double saldo;
    private ClienteDTO cliente;
}

