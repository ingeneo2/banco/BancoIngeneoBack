package org.example.bancoingeneoback.Model.DTO;

import lombok.Data;

@Data
public class ClienteDTO {
    private Long id;
    private String nombre;
    private String direccion;
    private String telefono;

}

