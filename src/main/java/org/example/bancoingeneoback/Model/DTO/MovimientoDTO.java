package org.example.bancoingeneoback.Model.DTO;

import lombok.Data;
import org.example.bancoingeneoback.Model.Enums.Tipo;

import java.util.Date;

@Data
public class MovimientoDTO {
    private Long id;
    private Tipo tipo;
    private Date fecha;
    private double valor;
    private CuentaDTO cuenta;
}

