package org.example.bancoingeneoback.Model.Entidades;

import jakarta.persistence.*;
import lombok.Data;
import org.example.bancoingeneoback.Model.Enums.Tipo;

import java.util.Date;
@Data
@Entity
public class Movimiento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private Tipo tipo;
    private Date fecha;
    private double valor;

    @ManyToOne
    @JoinColumn(name = "cuenta_id")
    private Cuenta cuenta;

}

