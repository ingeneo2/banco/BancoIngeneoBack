package org.example.bancoingeneoback.Config;

public class Constantes {

    public static final String CHUCK_NORRIS_API_URL = "https://api.chucknorris.io/jokes/random";
    public static final String KEY = "value";

    //Errores
    public static final String ERROR_OBTENER_CHISTE = "Error al obtener chiste de Chuck Norris";
    public static final String SALDO_NEGATIVO = "El saldo resultante sería negativo";

    //Cors
    public static final String URL_ANGULAR = "http://localhost:4200";
    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";
    public static final String API = "/api/**";
    public static final String ENCABEZADO = "*";
}
