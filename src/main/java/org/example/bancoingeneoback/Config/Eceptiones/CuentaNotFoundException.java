package org.example.bancoingeneoback.Config.Eceptiones;

public class CuentaNotFoundException extends RuntimeException {

    public CuentaNotFoundException(Long id) {
        super("Cuenta no encontrada con ID: " + id);
    }
}

