package org.example.bancoingeneoback.Config.Eceptiones;

public class SaldoInsuficienteException extends RuntimeException {

    public SaldoInsuficienteException(String message) {
        super(message);
    }
}

