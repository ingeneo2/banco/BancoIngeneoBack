package org.example.bancoingeneoback.Config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(Constantes.API)
                .allowedOrigins(Constantes.URL_ANGULAR) // Permite solicitudes desde este origen
                .allowedMethods(Constantes.GET, Constantes.POST,Constantes.PUT, Constantes.DELETE) // Métodos HTTP permitidos
                .allowedHeaders(Constantes.ENCABEZADO) // Todos los encabezados permitidos
                .allowCredentials(true); // Permitir credenciales (cookies, autorización, etc.)
    }
}

