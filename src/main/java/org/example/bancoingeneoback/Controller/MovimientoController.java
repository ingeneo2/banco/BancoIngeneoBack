package org.example.bancoingeneoback.Controller;

import org.example.bancoingeneoback.Model.DTO.ClienteDTO;
import org.example.bancoingeneoback.Model.DTO.MovimientoDTO;
import org.example.bancoingeneoback.Services.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/movimientos")
public class MovimientoController {

    @Autowired
    private MovimientoService movimientoService;

    @PostMapping("/{cuentaId}")
    public ResponseEntity<MovimientoDTO> registrarMovimiento(@PathVariable Long cuentaId, @RequestBody MovimientoDTO movimientoDTO) {
        MovimientoDTO movimientoRegistrado = movimientoService.registrarMovimiento(cuentaId, movimientoDTO);
        return new ResponseEntity<>(movimientoRegistrado, HttpStatus.CREATED);
    }
    @GetMapping
    public List<MovimientoDTO> obtenerTodosLosClientes(){
        return movimientoService.obtenerTodosLosMovimientos();
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarCliente(@PathVariable Long id) {
        movimientoService.eliminarMovimiento(id);
        return ResponseEntity.noContent().build();
    }
}

