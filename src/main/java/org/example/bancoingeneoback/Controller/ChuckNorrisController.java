package org.example.bancoingeneoback.Controller;

import org.example.bancoingeneoback.Services.ChuckNorrisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/chuck-norris")
public class ChuckNorrisController {

    private final ChuckNorrisService chuckNorrisService;

    @Autowired
    public ChuckNorrisController(ChuckNorrisService chuckNorrisService) {
        this.chuckNorrisService = chuckNorrisService;
    }

    @GetMapping("/joke")
    public ResponseEntity<String> obtenerChisteDeChuckNorris() {
        String chiste = chuckNorrisService.obtenerChisteDeChuckNorris();
        return ResponseEntity.ok(chiste);
    }
}

