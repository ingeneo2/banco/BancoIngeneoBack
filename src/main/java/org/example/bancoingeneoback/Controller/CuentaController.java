package org.example.bancoingeneoback.Controller;

import org.example.bancoingeneoback.Model.DTO.CuentaDTO;
import org.example.bancoingeneoback.Model.DTO.InformeClienteDTO;
import org.example.bancoingeneoback.Services.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/cuentas")
public class CuentaController {

    @Autowired
    private CuentaService cuentaService;

    @PostMapping("/{clienteId}")
    public ResponseEntity<CuentaDTO> crearCuenta(@PathVariable Long clienteId, @RequestBody CuentaDTO cuentaDTO) {
        CuentaDTO cuentaCreada = cuentaService.crearCuenta(clienteId, cuentaDTO);
        return new ResponseEntity<>(cuentaCreada, HttpStatus.CREATED);
    }
    @GetMapping
    public List<CuentaDTO> obtenerTodasLasCuentas(){
        return cuentaService.obtenerTodasLasCuentas();
    }
    @GetMapping("/{id}")
    public ResponseEntity<CuentaDTO> obtenerCuentaPorId(@PathVariable Long id) {
        CuentaDTO cuentaDTO = cuentaService.obtenerCuentaPorId(id);
        return ResponseEntity.ok(cuentaDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CuentaDTO> actualizarCuenta(@PathVariable Long id, @RequestBody CuentaDTO cuentaDTO) {
        CuentaDTO cuentaActualizada = cuentaService.actualizarCuenta(id, cuentaDTO);
        return ResponseEntity.ok(cuentaActualizada);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> eliminarCuenta(@PathVariable Long id) {
        cuentaService.eliminarCuenta(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{clienteId}/informe")
    public ResponseEntity<InformeClienteDTO> generarInformeCliente(
            @PathVariable Long clienteId,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaInicio,
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date fechaFin
    ) {
        InformeClienteDTO informeClienteDTO = cuentaService.generarInformeCliente(clienteId, fechaInicio, fechaFin);
        return ResponseEntity.ok(informeClienteDTO);
    }
}

